# Recipe App api proxy

NGINX proxy app for our video streaming platform

## Usage

### Environment variables

* `LISTEN_PORT` - Port to Listen on (default: `8000` )
* `APP_HOST` - Hostname to forward the requests to (default:`app`)
* `APP_PORT` -  POrt of the app to forward the requests to (default: `9000`)